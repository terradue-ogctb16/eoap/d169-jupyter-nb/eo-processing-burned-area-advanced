FROM terradue/l0-binder:latest

MAINTAINER Terradue S.r.l

COPY . ${HOME}

USER root

RUN /opt/anaconda/bin/conda env create --name env-binder --file ${HOME}/environment.yml

RUN /opt/anaconda/envs/env-binder/bin/python -m ipykernel install --name kernel

RUN chown -R ${NB_UID} ${HOME}
RUN mkdir -p /workspace/data && chown -R ${NB_UID} /workspace
USER ${NB_USER}

RUN test -f ${HOME}/postBuild && chmod 755 ${HOME}/postBuild && ${HOME}/postBuild

ENV PREFIX /opt/anaconda/envs/env-binder

WORKDIR ${HOME}