import os
import sys
#import pandas as pd
import otbApplication
#import lxml.etree as etree
import numpy as np
import math
from os.path import exists
import ogr
import gdal 
from osgeo.gdalconst import GA_ReadOnly
from ogr import osr
import geopandas as gpd
import json
import shutil
gdal.UseExceptions()  


def translate(vrt_bands, vrt, tif):
    
    ds = gdal.BuildVRT(vrt,
                       vrt_bands,
                       srcNodata=0,
                       xRes=10, 
                       yRes=10,
                       separate=True)
    ds.FlushCache()
    
    gdal.Translate(tif,
                   vrt,
                   outputType=gdal.GDT_UInt16)

def cog(input_tif):
    
    temp_tif = 'temp.tif'
    
    shutil.move(input_tif, temp_tif)
    
    translate_options = gdal.TranslateOptions(gdal.ParseCommandLine('-co TILED=YES ' \
                                                                    '-co COPY_SRC_OVERVIEWS=YES ' \
                                                                    ' -co COMPRESS=LZW'))

    ds = gdal.Open(temp_tif, gdal.OF_READONLY)

    gdal.SetConfigOption('COMPRESS_OVERVIEW', 'DEFLATE')
    ds.BuildOverviews('NEAREST', [2,4,8,16,32])
    
    ds = None

    ds = gdal.Open(temp_tif)
    gdal.Translate(input_tif,
                   ds, 
                   options=translate_options)
    ds = None

    os.remove('{}.ovr'.format(temp_tif))
    os.remove(temp_tif)

    
def write_tif(layer, output_name, width, height, input_geotransform, input_georef, format=gdal.GDT_Byte, to_cog=True):

    driver = gdal.GetDriverByName('GTiff')

    output = driver.Create(output_name, 
                           width, 
                           height, 
                           1, 
                           gdal.GDT_Byte) 
        
    output.SetGeoTransform(input_geotransform)
    output.SetProjection(input_georef)
    output.GetRasterBand(1).WriteArray(layer),

    output.FlushCache()

    if to_cog:
        
        cog(output_name)


def polygonize(url, field):
    
    ds = gdal.Open(url)
    
    srs = osr.SpatialReference(wkt=ds.GetProjection())
    
    band = ds.GetRasterBand(1)
    band_array = band.ReadAsArray()

    out_geojson = 'polygonized.json'

    driver = ogr.GetDriverByName('GeoJSON')

    out_data_source = driver.CreateDataSource(out_geojson + "")
    out_layer = out_data_source.CreateLayer('polygonized', srs=srs)

    new_field = ogr.FieldDefn(field, ogr.OFTInteger)
    out_layer.CreateField(new_field)

    gdal.Polygonize(band, None, out_layer, 0, [], callback=None )

    out_data_source = None
    ds = None

    data = json.loads(open(out_geojson).read())
    gdf = gpd.GeoDataFrame.from_features(data['features'])
    gdf = gdf[gdf[field] == 1]
        
    gdf.crs = {'init':'epsg:{}'.format(srs.GetAttrValue('AUTHORITY', 1))}
    
    os.remove(out_geojson)
    
    return gdf


def contrast_enhancement(in_tif, out_tif, hfact=1.0):

    ContrastEnhancement = otbApplication.Registry.CreateApplication("ContrastEnhancement")

    ContrastEnhancement.SetParameterString("in", in_tif)
    ContrastEnhancement.SetParameterString("out", out_tif)
    ContrastEnhancement.SetParameterOutputImagePixelType("out", otbApplication.ImagePixelType_uint8)
    ContrastEnhancement.SetParameterFloat("nodata", 0.0)
    ContrastEnhancement.SetParameterFloat("hfact", hfact)
    ContrastEnhancement.SetParameterInt("bins", 256)
    ContrastEnhancement.SetParameterInt("spatial.local.w", 500)
    ContrastEnhancement.SetParameterInt("spatial.local.h", 500)
    ContrastEnhancement.SetParameterString("mode","lum")

    ContrastEnhancement.ExecuteAndWriteOutput()

    return True 





    
    

    